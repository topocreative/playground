package org.hotovo.playground

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import org.hotovo.playground.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        // jetpack view binding
        binding.buttonBinding.setOnClickListener {
            Toast.makeText(this, "You used Jetpack view binding", Toast.LENGTH_SHORT).show()
        }
        // old way KTX syntetic
        button_syntetics.setOnClickListener {
            Toast.makeText(this, "You used KTX syntetic", Toast.LENGTH_SHORT).show()
        }

        binding.switchColor.setOnCheckedChangeListener { _, checked ->
            binding.mainScreenContainer.setBackgroundColor(
                if (checked)
                    ContextCompat.getColor(this, R.color.yellow)
                else
                    ContextCompat.getColor(this, R.color.white)
            )
        }

        binding.buttonGoToSub.setOnClickListener {
            startActivity(Intent(this, SubActivity::class.java))
        }

    }
}