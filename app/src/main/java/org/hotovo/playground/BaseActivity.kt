package org.hotovo.playground

import android.app.Activity
import android.os.Bundle
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<VB : ViewBinding>() : Activity() {

    protected lateinit var binding: VB

    abstract fun inflateBinding(): VB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = inflateBinding()
        setContentView(binding.root)
    }
}