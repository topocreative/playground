package org.hotovo.playground

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import org.hotovo.playground.databinding.ActivitySubBinding

class SubActivity : BaseActivity<ActivitySubBinding>() {

    override fun inflateBinding(): ActivitySubBinding {
        return ActivitySubBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.buttonBinding.setOnClickListener {
            Toast.makeText(this, "Yes I am working", Toast.LENGTH_SHORT).show()
        }
        binding.switchColor.setOnCheckedChangeListener { _, checked ->
            binding.mainScreenContainer.setBackgroundColor(
                if (checked)
                    ContextCompat.getColor(this, R.color.yellow)
                else
                    ContextCompat.getColor(this, R.color.white)
            )
        }
    }

}